# Samurai Sword Card Game TTS Mod

#### Work in progress...

## Housekeeping/FAQ
1. The "Saves" directory *should* work out of box. Follow instructions in sub-folder readme. 
2. The Global.lua file in /src/ can be copied/pasted in any TTM game. Although, the zones are currently only mapped to the 'Kraken Table' asset which can be found on the steam workshop. 

## Current Dependencies: 
[Kraken Table](https://steamcommunity.com/sharedfiles/filedetails/?id=1222553243) by [Lily](https://steamcommunity.com/id/Liviane)

## License
[TableTopSimulator](https://www.tabletopsimulator.com/) is the intellectual property of Berserk games. 

[Samurai Sword](https://www.dvgiochi.com/catalogo/samurai-sword/) and its expansion, [Samurai Sword: Rising Sun](https://www.dvgiochi.com/catalogo/samurai-sword-rising-sun/?linea=1/?lang=ita), are the intellectual property of [daVinci Editrice S.r.l](https://www.dvgiochi.com/).

This mod is in no way associated with Berserk games or daVinci Editrice S.r.l. All assets, scripts and files in this repo are available under the MIT lisence. Any assets, scripts or files which conflict with Canadian and/or International copyright law will be removed upon request. 

<br/> 

MIT License

Copyright (c) 2023 Brian Jenkins et al.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
